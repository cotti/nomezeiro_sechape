﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nomezeirochape.Util;
using Nomezeirochape.Service;
using System.Windows.Input;
using Microsoft.Win32;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace Nomezeirochape.Main
{
    public class ViewModelMain : ViewModelBase
    {
        #region Fields

        private string destinationFile;
        private int amountmiddleNames;
        private bool middleNames;
        private bool integrationIdentifier;
        private string identifierString;
        private bool fileNumber;
        private string fileNumberString;
        private int quantity;
        private string typedNumber;
        private ObservableCollection<string> phones = new ObservableCollection<string>();

        private ICommand selectDirCommand;
        private ICommand addElementCommand;
        private ICommand remElementCommand;
        private ICommand generateCallListCommand;

        #endregion

        #region Properties

        public string DestinationFile
        {
            get { return destinationFile; }
            set
            {
                destinationFile = value;
                RaisePropertyChanged(nameof(DestinationFile));
            }
        }

        public ICommand SelectDirCommand
        {
            get
            {
                return selectDirCommand;
            }

            set
            {
                selectDirCommand = value;
                RaisePropertyChanged(nameof(SelectDirCommand));
            }
        }

        public int AmountMiddleNames
        {
            get
            {
                return amountmiddleNames;
            }

            set
            {
                amountmiddleNames = value;
                RaisePropertyChanged(nameof(AmountMiddleNames));
            }
        }

        public bool MiddleNames
        {
            get
            {
                return middleNames;
            }

            set
            {
                middleNames = value;
                RaisePropertyChanged(nameof(MiddleNames));
            }
        }

        public bool IntegrationIdentifier
        {
            get
            {
                return integrationIdentifier;
            }

            set
            {
                integrationIdentifier = value;
                RaisePropertyChanged(nameof(IntegrationIdentifier));
            }
        }

        public bool FileNumber
        {
            get
            {
                return fileNumber;
            }

            set
            {
                fileNumber = value;
                RaisePropertyChanged(nameof(FileNumber));
            }
        }

        public bool CanGenerate
        {
            get
            {
                return quantity > 0 && Phones.Count != 0;
            }

            set
            {
                RaisePropertyChanged(nameof(CanGenerate));
            }
        }

        public string FileNumberString
        {
            get
            {
                return fileNumberString;
            }

            set
            {
                fileNumberString = value;
                RaisePropertyChanged(nameof(FileNumberString));
            }
        }

        public string IdentifierString
        {
            get
            {
                return identifierString;
            }

            set
            {
                identifierString = value;
                RaisePropertyChanged(nameof(IdentifierString));
            }
        }

        public ObservableCollection<string> Phones
        {
            get
            {
                return phones;
            }

            set
            {
                phones = value;
                RaisePropertyChanged(nameof(Phones));
                RaisePropertyChanged(nameof(CanGenerate));

            }
        }

        public string Quantity
        {
            get
            {
                return quantity.ToString();
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                    quantity = 0;
                else
                {
                    if (quantity != 0)
                        quantity = int.Parse(value);
                    else
                        quantity = value.Substring(0, 1) != "0" ? int.Parse(value.Substring(0, 1)) : int.Parse(value.Substring(1, 1));
                }
                RaisePropertyChanged(nameof(Quantity));
                RaisePropertyChanged(nameof(CanGenerate));

            }
        }

        public string TypedNumber
        {
            get
            {
                return typedNumber;
            }

            set
            {
                typedNumber = value;
                RaisePropertyChanged(nameof(TypedNumber));
            }
        }

        public ICommand AddElementCommand
        {
            get
            {
                return addElementCommand;
            }

            set
            {
                addElementCommand = value;
                RaisePropertyChanged(nameof(AddElementCommand));
            }
        }

        public ICommand RemElementCommand
        {
            get
            {
                return remElementCommand;
            }

            set
            {
                remElementCommand = value;
                RaisePropertyChanged(nameof(RemElementCommand));
            }
        }

        public ICommand GenerateCallListCommand
        {
            get
            {
                return generateCallListCommand;
            }

            set
            {
                generateCallListCommand = value;
                RaisePropertyChanged(nameof(GenerateCallListCommand));
            }
        }

        #endregion

        #region Methods

        public ViewModelMain()
        {
            SelectDirCommand = new RelayCommand(SelectDir);
            AddElementCommand = new RelayCommand(AddElement);
            RemElementCommand = new RelayCommand(RemoveElement);
            GenerateCallListCommand = new RelayCommand(GenerateCallList,param => CanGenerate);
            DestinationFile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\calllist.csv";
            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime)
            {
                DesignModeHelper.MainViewDummy(ref destinationFile, ref integrationIdentifier, ref fileNumber, ref phones);
            }
            CallListWriterService.LoadFile();
        }

        private void GenerateCallList(object obj)
        {
            CallListWriterService.CreateFile(MiddleNames, IntegrationIdentifier, FileNumber, AmountMiddleNames, quantity, DestinationFile, IdentifierString, FileNumberString, Phones.ToList());
        }

        private void RemoveElement(object obj)
        {
            if (obj != null)
                Phones.Remove((string)obj);
        }

        private void AddElement(object obj)
        {
            if (TypedNumber != null)
                Phones.Add(TypedNumber);
        }

        private void SelectDir(object obj)
        {
            SaveFileDialog Dialog = new SaveFileDialog();
            Dialog.Title = "Call List save folder";
            Dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            Dialog.Filter = "CSV file (*.csv)|*.csv";
            Dialog.DefaultExt = ".csv";

            if (Dialog.ShowDialog() == true)
            {
                DestinationFile = Dialog.FileName;
            }
        }

        #endregion
    }
}
