﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace Nomezeirochape.Service
{
    public class CallListWriterService
    {
        private static List<string> FirstNamesList = new List<string>();
        private static List<string> MiddleNamesList = new List<string>();
        private static List<string> LastNamesList = new List<string>();
        private static dynamic JSON;
        private static StreamWriter CallList;

        public delegate void NameFileLoadedHandler ();
        public static event NameFileLoadedHandler NameFileLoaded;

        protected virtual void OnNameFileLoaded()
        {
            (NameFileLoaded as NameFileLoadedHandler)?.Invoke();
        }

        public static void LoadFile()
        {
            JSON = JsonConvert.DeserializeObject(Encoding.UTF8.GetString(Properties.Resources.data));

            foreach (var a in JSON.firstname)
            {
                FirstNamesList.Add((string)a);
            }
            foreach (var a in JSON.middlename)
            {
                MiddleNamesList.Add((string)a);
            }
            foreach (var a in JSON.lastname)
            {
                LastNamesList.Add((string)a);
            }
        }

        public static void CreateFile(bool MiddleNames, bool IntegrationIdentifier, bool Filenumber, int AmountMiddleNames, int Quantity, 
                                      string DestinationFile, string IdentifierString, string FilenumberString, List<string> Phones)
        {
            int i = 1;
            CallList = new StreamWriter(DestinationFile);
            Random NameRandom = new Random();
            
            //Header
            CallList.Write("Firstname,");
            if (MiddleNames)
                CallList.Write("Middlename,");
            CallList.Write("Lastname,");
            if (IntegrationIdentifier)
                CallList.Write("IntegrationIdentifier,");
            if (Filenumber)
                CallList.Write("Filenumber,");
            for(; i < Phones.Count; i++)
            {
                CallList.Write($"Phone number {i},");
            }
            CallList.Write($"Phone number {i}");
            CallList.WriteLine();

            //Body
            for (i = 0; i < Quantity; i++)
            {
                CallList.Write(FirstNamesList.ElementAt(NameRandom.Next(0, FirstNamesList.Count - 1)) + ",");
                if (MiddleNames)
                {
                    string concatmiddle = string.Empty;
                    for (int a = 0; a < NameRandom.Next(0, (AmountMiddleNames - 1)); a++)
                    {
                        concatmiddle += (MiddleNamesList.ElementAt(NameRandom.Next(0, MiddleNamesList.Count - 1)) + " ");
                    }
                    concatmiddle += (MiddleNamesList.ElementAt(NameRandom.Next(0, MiddleNamesList.Count - 1)));
                    CallList.Write(concatmiddle + ",");
                }
                CallList.Write(LastNamesList.ElementAt(NameRandom.Next(0, LastNamesList.Count - 1)) + ",");
                if (IntegrationIdentifier)
                    CallList.Write(IdentifierString + ",");
                if (Filenumber)
                    CallList.Write(FilenumberString + ",");
                for (int p = 0; p < (Phones.Count - 1); p++)
                {
                    CallList.Write(Phones.ElementAt(p) + ",");
                }
                CallList.Write(Phones.Last());
                CallList.WriteLine();
            }

            CallList.Close();

        }

    }
}
